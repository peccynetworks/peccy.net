import CommonHead from '../components/CommonHead'
import Navbar from '../components/Navbar'

export default () => (
  <div>
    <CommonHead/>
    <section className="hero is-info is-fullheight">
      <div className="hero-head">
        <Navbar/>
      </div>

      <div className="hero-body">
        <div className="container has-text-centered">
          <h1 className="title">
            We're Peccy Networks
          </h1>
          <h2 className="subtitle">
            The IT platform powered by <i>curiosity</i> and <u>innovation</u>.
          </h2>
        </div>
      </div>

      <div className="hero-foot has-text-centered">
        <p>&copy; Peccy Networks 2014-2018</p>
      </div>
    </section>
	</div>
)
